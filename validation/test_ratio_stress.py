import os
from protoplast.ellispoid.mechanics import simulate, hyperelasticity
from protoplast.postprocessing import evaluate_tensor

from femtk.utils.mechanics import parameters_convertor, move_mesh, compute_cauchy
from femtk.utils.io import load_mesh

import matplotlib.pyplot as plt

import fenics as fe

from femtk import logger
import numpy as np

import logging

pwd = os.path.dirname(os.path.abspath(__file__))

files = [f for f in os.listdir(pwd+'/meshes') if '.xdmf' in f]
files= sorted(files)

logger.setLevel(logging.WARNING)

ey = np.array([0,1,0])
ez = np.array([0,0,1])
pressure = 0.01
young = 100
poisson = 0.1


def theo_stress_meridio(b, p):
    e=1
    return (b*p)/(2*e)

def theo_stress_circum(a,b,p):
    return theo_stress_meridio(b,p)*(2-(b**2/a**2))


def eval_triangle_area(mesh, ind):
    return fe.Cell(mesh, ind).volume()


stresses_comp = []
ratio_elli = []

print('-'*50)
for file_mesh in files:
    print('load file:\t', file_mesh)
    mesh = load_mesh('meshes/'+file_mesh)
    a = mesh.coordinates()[:,2].max()
    b = mesh.coordinates()[:,0].max()
    print('Length (a, b):\t', a, b)
    print('Ratio b/a before equilibrium:\t', b/a)

    mu = parameters_convertor('shear', plane_stress=False, young=young, poisson=poisson)
    lmbda = parameters_convertor('lame', plane_stress=False, young=young, poisson=poisson)

    disp = hyperelasticity(mesh, pressure, mu, lmbda, bnd_cond='nothing')

    ind_max_x = np.argmax(mesh.coordinates()[:,0])
    mesh.init(0,2)
    area_tri_before = sum([fe.Cell(mesh, t.index()).volume() for t in fe.cells(fe.Vertex(mesh, ind_max_x))])

    move_mesh(mesh, disp)

    with fe.XDMFFile(file_mesh.replace('.xdmf', '_moved.xdmf')) as f:
        f.write(mesh)

    a = mesh.coordinates()[:,2].max()
    b = mesh.coordinates()[:,0].max()
    print('Ratio b/a at equilibrium:\t', b/a, '\n')

    T = fe.TensorFunctionSpace(mesh, 'DG', 0)
    cauchy = fe.Function(T, name="cauchy")
    expr_cauchy = compute_cauchy(disp, mu, lmbda, projection=False)
    fe.project(expr_cauchy, T, function=cauchy)

    # ind_max_x = np.argmax(mesh.coordinates()[:,0])
    point = mesh.coordinates()[ind_max_x,:]
    # print(point)
    tensor = evaluate_tensor(point, cauchy)
    eigval, eigvec = np.linalg.eig(tensor)
    print('Eigenvalues:\t', eigval, "\n")
    # print("Ratio:\t",eigval[2]/eigval[1], '\n')

    norm_force_ey = np.linalg.norm(tensor.dot(ey))
    theo_circum = theo_stress_circum(a, b, pressure)
    # print("Force ey:\t", tensor.dot(ey))
    print("Stress circum computed:\t", norm_force_ey)
    print("Stress circum theo:\t", theo_circum)
    print("Error stress circum:\t", abs(theo_circum-norm_force_ey)/abs(theo_circum))

    print('')
    norm_force_ez = np.linalg.norm(tensor.dot(ez))
    theo_merid = theo_stress_meridio(b, pressure)
    # print("Force ez:\t", tensor.dot(ez))
    print("Stress merid computed:\t", norm_force_ez)
    print("Stress merid theo:\t", theo_merid)
    print("Error stress merid:\t", abs(theo_merid-norm_force_ez)/abs(theo_merid))

    print('')
    print("Ratio circum/meridio:\t", norm_force_ey/norm_force_ez)

    print('')
    mesh.init(0,2)
    area_tri_after = sum([fe.Cell(mesh, t.index()).volume() for t in fe.cells(fe.Vertex(mesh, ind_max_x))])
    print('Area triangles before:\t', area_tri_before)
    print('Area triangles after:\t', area_tri_after)
    if np.sign(area_tri_after-area_tri_before)>0:
        print('--> Tension:\t', area_tri_after-area_tri_before)
    else:
        print('--> Compression:\t', area_tri_after-area_tri_before)
    print('-'*50)

    stresses_comp.append([norm_force_ey, norm_force_ez])
    ratio_elli.append(b/a)

stresses_comp = np.asarray(stresses_comp)
ratio_elli = np.asarray(ratio_elli)

ind = np.argsort(ratio_elli)
stresses_comp = stresses_comp[ind]
ratio_elli = ratio_elli[ind]

stresses_theo = np.array([[bval, theo_stress_circum(1, bval, pressure), theo_stress_meridio(bval, pressure)] for bval in np.linspace(0.1, 4, 100)])

fig0 = plt.figure(0)
fig0.gca().plot(ratio_elli, stresses_comp[:,0], 'b+', label='computed')
fig0.gca().plot(stresses_theo[:, 0], stresses_theo[:,1], 'r--', label='theoric')
plt.xlabel('ratio a/b')
plt.ylabel('circumferiental stress')
plt.legend()
plt.grid()

fig1 = plt.figure(1)
fig1.gca().plot(ratio_elli, stresses_comp[:,1], 'b+', label='computed')
fig1.gca().plot(stresses_theo[:, 0], stresses_theo[:,2], 'r--', label='theoric')
plt.xlabel('ratio a/b')
plt.ylabel('meridional stress')
plt.legend()
plt.grid()


plt.show()
