# Protoplast

Collection of scripts to launch mechanical simulation on free and confined protoplast-like meshes.

<p align="center">
<img src="static/meshes.png" width="600"/>
</p>

## Binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fmosaic%2Fpublications%2Fprotoplast/master)

If you want to test the code, you can use Binder to remotly launch the Jupyter Notebook example by cliking on the badge above.
(Simulation might take a long time)

## Install

If you want to use the source code, clone the git repository and use conda to install all the dependency:

To install miniconda: https://docs.conda.io/en/latest/miniconda.html

```bash
conda create -f environnement.yml
conda activate protoplast
```

And now you are ready to use the code.
