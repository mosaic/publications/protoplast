import time

import numpy as np

import fenics as fe
from femtk.boundary_conditions.dirichlet import ConstantDirichlet
from femtk.boundary_conditions.domains import Domain
from femtk.problems.elastoplasticity import HyperElasticityProblem
from femtk.utils.mechanics import (compute_cauchy, compute_PK2, compute_strain,
                                   move_mesh, pressure_forces, parameters_convertor)
from femtk.utils.tensor import project_sym_tensor

def _edges_dirichlet(problem):
    coords = problem.mesh.coordinates()
    ymin = coords[:,1].min()
    ymax = coords[:,1].max()


    diri = []
    # d1 = Domain('border')
    d1 = Domain(f'near(y, {ymin})') | Domain(f'near(y, {ymax})')
    diri.append(ConstantDirichlet((0,0,0), d1))

    problem.add_boundary_condition(diri)


def _vertex_dirichlet(problem):
    coords = problem.mesh.coordinates()
    xmin = coords[:,0].min()
    xmax = coords[:,0].max()
    ymin = coords[:,1].min()
    ymax = coords[:,1].max()

    diri = []
    d1 = Domain(f'near(x,{xmin})') & Domain(f'near(y,{ymin})')
    diri.append(ConstantDirichlet((0,0,0), d1, method='pointwise'))

    d2 = Domain(f'near(x,{xmin})') & Domain(f'near(y,{ymax})')
    diri.append(ConstantDirichlet((0,0,0), d2, method='pointwise'))

    d3 = Domain(f'near(x,{xmax})') & Domain(f'near(y,{ymax})')
    diri.append(ConstantDirichlet((0,0,0), d3, method='pointwise'))

    d4 = Domain(f'near(x,{xmax})') & Domain(f'near(y,{ymin})')
    diri.append(ConstantDirichlet((0,0,0), d4, method='pointwise'))

    problem.add_boundary_condition(diri)


def _build_mesh(x0, y0, x1, y1, nx, ny):
    comm = fe.MPI.comm_world
    mesh =  fe.RectangleMesh(comm,fe.Point(x0, y0), fe.Point(x1, y1), nx, ny)

    coords = mesh.coordinates()
    coords = np.c_[coords, np.zeros(len(coords))]
    mesh.coordinates()[:] = coords

    return mesh


def hyperelasticity(mesh, pressure, mu, lmbda, bnd_cond='vertex'):
    problem = HyperElasticityProblem()
    problem.solver_parameter(absolute_tolerance=1e-9, monitor_convergence=False)
    problem.set_mesh(mesh)
    problem.set_function_space('P', 2)

    if bnd_cond=="edges":
        _edges_dirichlet(problem)
    elif bnd_cond=='vertex':
        _vertex_dirichlet(problem)

    problem.set_material_model('st_venant', mu, lmbda)

    P = pressure_forces(problem.mesh, pressure, interior=[0,0,-1])
    problem.set_body_force(P)

    problem.solve()

    return problem.solution
