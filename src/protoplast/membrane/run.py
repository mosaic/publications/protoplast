from protoplast.membrane.mechanics import hyperelasticity
from protoplast.postprocessing import measurement_orientation, compute_anisotropy

from femtk.utils.io import load_mesh
from femtk.utils.tensor import project_sym_tensor
from femtk.utils.mechanics import (compute_cauchy, compute_PK2, compute_strain,
                                   move_mesh, pressure_forces, parameters_convertor)
import fenics as fe
from femtk import logger

import logging

# logger.setLevel(logging.WARNING)

file = "../../../mesh/membrane_21.xdmf"
result_dir = "../../../results/membrane"

bnd_cond = "vertex"
pressure = 10
young = 1000
poisson = .5

mu = parameters_convertor('shear', plane_stress=True, young=young, poisson=poisson)
lmbda = parameters_convertor('lame', plane_stress=True, young=young, poisson=poisson)

mesh = load_mesh(file)

disp = hyperelasticity(mesh, pressure, mu, lmbda, bnd_cond=bnd_cond)

with fe.XDMFFile(result_dir+'/disp.xdmf') as f:
    f.write_checkpoint(disp, "disp", 0)


T = fe.TensorFunctionSpace(mesh, 'DG', 0)

strain = fe.project(compute_strain(disp), T)
with fe.XDMFFile(result_dir+'/strain.xdmf') as f:
    f.write_checkpoint(strain, "strain", 0)

move_mesh(mesh, disp)

T = fe.TensorFunctionSpace(mesh, 'DG', 0)
cauchy = fe.Function(T, name="cauchy")
expr_caucchy = compute_cauchy(disp, mu, lmbda, projection=True)
fe.project(expr_caucchy, T, function=cauchy)

with fe.XDMFFile(result_dir+'/cauchy.xdmf') as f:
    f.write_checkpoint(cauchy, "cauchy", 0)

with fe.XDMFFile(result_dir+'/cauchy_sym.xdmf') as f:
    sym_cauchy = project_sym_tensor(cauchy, mesh, function_type='DG', function_degree=0, name='cauchy')
    f.write(sym_cauchy, 0)

aniso_cauchy = compute_anisotropy(cauchy)

with fe.XDMFFile(result_dir+'/anisotropy.xdmf') as f:
    f.write_checkpoint(aniso_cauchy, "anisotropy", 0)

orientation_cauchy = measurement_orientation(cauchy)

with fe.XDMFFile(result_dir+'/orientation.xdmf') as f:
    f.write_checkpoint(orientation_cauchy, "orientation", 0)

with fe.XDMFFile(result_dir+'/mesh.xdmf') as f:
    f.write(mesh)
