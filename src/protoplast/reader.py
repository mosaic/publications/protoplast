import fenics as fe

def read_displacement(file_disp, file_mesh, func_space="P", degree=2):
    mesh = fe.Mesh()

    with fe.XDMFFile(file_mesh) as f:
        f.read(mesh)

    V = fe.VectorFunctionSpace(mesh, func_space, degree)
    disp = fe.Function(V)

    with fe.XDMFFile(file_disp) as f:
        f.read_checkpoint(disp, 'displacement', 0)

    return disp


def read_cauchy(file_cauchy, file_mesh, func_space='DG', degree=0):
    mesh = fe.Mesh()

    with fe.XDMFFile(file_mesh) as f:
        f.read(mesh)

    T = fe.TensorFunctionSpace(mesh, func_space, degree)
    cauchy = fe.Function(T)

    with fe.XDMFFile(file_cauchy) as f:
        f.read_checkpoint(cauchy, 'cauchy', 0)

    return cauchy


def read_anisotropy(file_anisotropy, file_mesh, func_space='DG', degree=0):
    mesh = fe.Mesh()

    with fe.XDMFFile(file_mesh) as f:
        f.read(mesh)

    V = fe.FunctionSpace(mesh, func_space, degree)
    ani = fe.Function(V)

    with fe.XDMFFile(file_anisotropy) as f:
        f.read_checkpoint(ani, 'anisotropy', 0)

    return ani
