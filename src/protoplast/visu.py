import fenics as fe
import numpy as np
import vtk

from visu_core.vtk.actor import (vtk_actor, vtk_display_actors,
                                 vtk_display_notebook,
                                 vtk_display_actor)
from visu_core.vtk.polydata import (edge_scalar_property_polydata,
                                    face_scalar_property_polydata,
                                    vertex_vector_property_polydata,
                                    vertex_tensor_property_polydata)


def _plot_mesh_edges(mesh, colormap='gray_r', glyph_scale=10):
    # array = function.compute_vertex_value()
    # mesh = function.function_space().mesh()


    vertex = mesh.coordinates()
    edges = np.array([list(v.entities(0)) for v in fe.edges(mesh)])

    polydata = edge_scalar_property_polydata(vertex, edges, np.ones(len(edges)), glyph_scale=glyph_scale)

    actor = vtk_actor(polydata, colormap=colormap)
    mapedge = actor.GetMapper()
    mapedge.SetResolveCoincidentTopologyPolygonOffsetParameters(1,1)
    mapedge.SetResolveCoincidentTopologyToPolygonOffset()
    mapedge.ScalarVisibilityOff()
    actor.GetProperty().SetAmbient(1.0)
    actor.GetProperty().SetDiffuse(0.0)
    actor.GetProperty().SetSpecular(0.0)


    return [actor]

def _plot_mesh_surfaces(mesh, colormap='gray', opacity=1):
    vertex = mesh.coordinates()
    if len(vertex[0,:])==2:
        vertex = np.c_[vertex, np.zeros(len(vertex[:,0]))]
    cells = mesh.cells()

    polydata = face_scalar_property_polydata(vertex, cells, np.ones(len(cells)))
    actor = vtk_actor(polydata, colormap=colormap, opacity=opacity)

    mapsurf = actor.GetMapper()
    mapsurf.SetResolveCoincidentTopologyPolygonOffsetParameters(0,1)
    mapsurf.SetResolveCoincidentTopologyToPolygonOffset()

    return [actor]

def _plot_vector_function(function, scale=1):
    ar = function.compute_vertex_values().reshape(-1,function.ufl_shape[0])*scale
    coord = function.function_space().mesh().coordinates()

    polydata = vertex_vector_property_polydata(coord, ar)
    actor = vtk_actor(polydata)

    return [actor]


def display_mesh(mesh):
    vtk_display_actors(_plot_mesh_surfaces(mesh)+_plot_mesh_edges(mesh))

def display_mesh_notebook(mesh):
    return vtk_display_notebook(_plot_mesh_surfaces(mesh)+_plot_mesh_edges(mesh), background=(0, 0, 0))

def _plot_crosshair(tensor, glyph="crosshair", colormap='viridis', glyph_scale=1, decimate=False, value_range=None):

    mesh = tensor.function_space().mesh()
    if decimate:
        center_tri, ar = decimate_tensor(tensor)
    else:
        ar = tensor.vector().get_local().reshape(-1,3,3)
        center_tri = np.array([c.midpoint().array() for c in fe.cells(mesh)])
    poly = vertex_tensor_property_polydata(center_tri, ar)
    glyph = _glyph_tensor(poly, glyph_scale=glyph_scale)
    if value_range is None:
        actor = vtk_actor(glyph, colormap=colormap)
    else:
        actor = vtk_actor(glyph, colormap=colormap, value_range=value_range)

    return [actor]

def _glyph_tensor(input_polydata, radius=None, height=1, resolution=16, glyph_scale=1):
    glyph = vtk.vtkTensorGlyph()

    line = vtk.vtkCylinderSource()
    line.SetCenter(0.0, 0.0, 0.0)
    if radius is None:
        line.SetRadius(0.05/np.sqrt(glyph_scale))
    else:
        line.SetRadius(radius)
    line.SetHeight(height)
    line.SetResolution(resolution)
    line.Update()

    rotation = vtk.vtkTransform()
    rotation.RotateZ(90)
    rotation_filter = vtk.vtkTransformPolyDataFilter()
    rotation_filter.SetInputConnection(line.GetOutputPort())
    rotation_filter.SetTransform(rotation)

    translation = vtk.vtkTransform()
    translation.Translate(0.5,0,0)
    translation_filter = vtk.vtkTransformPolyDataFilter()
    translation_filter.SetInputConnection(rotation_filter.GetOutputPort())
    translation_filter.SetTransform(translation)

    glyph.SetSourceConnection(translation_filter.GetOutputPort())
    glyph.ThreeGlyphsOn()

    glyph.SetInputData(input_polydata)
    glyph.ColorGlyphsOn()
    glyph.SetColorModeToEigenvalues()
    glyph.SymmetricOn()
    glyph.SetScaleFactor(glyph_scale)
    glyph.ExtractEigenvaluesOn()

    glyph.Update()
    polydata = glyph.GetOutput()

    return polydata




def cell_neigbors(cell):
    cells_ind = [c.index() for e in fe.edges(cell) for c in fe.cells(e) if c.index() != cell.index()]
    return cells_ind


def mesh_coloring(mesh):
    cell_list = np.arange(mesh.num_entities(2)).tolist()
    zones = []
    while len(cell_list)!=0:
        ind = cell_list[0]
        neighbors = cell_neigbors(fe.Cell(mesh, ind))

        zone = [ind] + neighbors
        zones.append(zone)

        cell_list.remove(ind)
        for i in neighbors:
            try:
                cell_list.remove(i)
            except:
                pass

    return zones


def decimate_tensor(tensor):
    mesh = tensor.function_space().mesh()
    array =c
    center_tri = np.array([c.midpoint().array() for c in fe.cells(mesh)])
    cell_list = np.arange(mesh.num_entities(2)).tolist()
    to_remove = []
    while len(cell_list)!=0:
        ind = cell_list[0]
        neighbors = cell_neigbors(fe.Cell(mesh, ind))

        to_remove += neighbors
        # zone = [ind] + neighbors
        # for i in zone[1:]:
        #     array[i] = np.zeros((3,3))

        cell_list.remove(ind)
        for i in neighbors:
            try:
                cell_list.remove(i)
            except:
                pass

    to_remove = np.unique(to_remove)
    array = np.delete(array, to_remove, axis=0)
    center_tri = np.delete(center_tri, to_remove, axis=0)

    # tensor.vector().set_local(array.flatten())

    return center_tri, array

def normalize(vector):
    return vector/np.linalg.norm(vector)

def measurement_principal_axis(tensor, eps=1e-4):
    array = tensor.vector().get_local().reshape(-1, 3, 3)
    eigval, eigvec = np.linalg.eigh(array)
    eigval_2 = eigval[:, 1:]
    eigvec_2 = eigvec[:, :, 1:]

    p = np.array([v1*e1 - e2*v1 if np.linalg.norm(v1*e1 - e2*v1)>eps else np.zeros(3) for v1, e2, e1 in  zip(eigvec_2[:,:,1],eigval_2[:,0], eigval_2[:,1])])

    V = fe.VectorFunctionSpace(tensor.function_space().mesh(), 'DG', 0)
    f = fe.Function(V, name='orientation')
    f.vector().set_local(np.array(p.flatten()))

    return f

if __name__ == "__main__":
    mesh = fe.Mesh()
    with fe.XDMFFile('../mesh/protoplast_ratio_32.xdmf') as f:
        f.read(mesh)

    T = fe.TensorFunctionSpace(mesh, 'DG', 0)
    cauchy = fe.Function(T)

    with fe.XDMFFile('../results/no_walls/ratio_32/cauchy_0.xdmf') as f:
        f.read_checkpoint(cauchy, 'cauchy', 0)

    # decimate_tensor(cauchy)
    orientation = measurement_principal_axis(cauchy)

    with fe.XDMFFile('orientation.xdmf') as f:
        f.write_checkpoint(orientation, 'orientation', 0)

    # vtk_display_actor(_plot_crosshair(cauchy, glyph_scale=2e-3, colormap='gray', decimate=True, value_range=[0,0])[0], set_scalar_bar=False)
