from cellmesh.meshing import generateMesh
from cellmesh.converter import convert_to_fenics
from cellmesh.geometry.basic import Ellipsoid
import cellmesh

import os


def generate_mesh(ratio_a, ratio_b, size=0.1, path='.', verbose=True):

    cellmesh.initialize(verbose=verbose)
    circle = Ellipsoid()
    circle.generate(ratio_a, ratio_b)

    generateMesh(circle, size, path+'/ellipsoid.msh', algorithm='MeshAdapt')
    convert_to_fenics(path+'/ellipsoid.msh', path+'/protoplast_ratio_'+str(ratio_a)+'_'+str(ratio_b)+'.xdmf')

    os.remove(path+'/ellipsoid.msh')

    cellmesh.finalize()
