from protoplast.ellispoid.mechanics import simulate

from femtk.utils.mechanics import parameters_convertor
from femtk.utils.io import load_mesh

from femtk import logger

import logging


logger.setLevel(logging.WARNING)

file = "../../../mesh/pill.xdmf"

pressure = 10
young = 1000
poisson = .5

mu = parameters_convertor('shear', plane_stress=True, young=young, poisson=poisson)
lmbda = parameters_convertor('lame', plane_stress=True, young=young, poisson=poisson)

mesh = load_mesh(file)

simulate(mesh, pressure, mu, lmbda, repeat=1, bnd_cond="no_walls", result_dir='../../../results/pill')
