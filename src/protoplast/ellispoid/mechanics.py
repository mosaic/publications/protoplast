import time

import fenics as fe
import numpy as np

from femtk.boundary_conditions.dirichlet import ConstantDirichlet
from femtk.boundary_conditions.domains import Domain
from femtk.problems.elastoplasticity import HyperElasticityProblem
from femtk.utils.mechanics import (compute_cauchy, compute_PK2, compute_strain,
                                   move_mesh, parameters_convertor,
                                   pressure_forces)
from femtk.utils.tensor import project_sym_tensor
from protoplast.postprocessing import (compute_anisotropy, evaluate_tensor,
                                       measurement_orientation)


def no_walls(problem):
    diri = []
    d1 = Domain('near(x,0)')
    diri.append(ConstantDirichlet(0, d1, subspace=0))
    d2 = Domain('near(y,0)')
    diri.append(ConstantDirichlet(0, d2, subspace=1))
    d3 = Domain('near(z,0)')
    diri.append(ConstantDirichlet(0, d3, subspace=2))

    problem.add_boundary_condition(diri)


def walls2(problem, pos=1, eps=0.001):
    diri = []

    walls = fe.DirichletBC(problem.functionSpace.sub(0), fe.Constant(0), lambda x, on_boundary: not fe.between(x[0], (-pos+eps, pos-eps)))
    problem.add_boundary_condition(walls)

    d3 = Domain("near(y,0)")
    diri.append(ConstantDirichlet(0, d3, subspace=1))
    d4 = Domain('near(z,0)')
    diri.append(ConstantDirichlet(0, d4, subspace=2))

    problem.add_boundary_condition(diri)


def walls4(problem, pos=1, eps=0.001):
    diri = []

    walls_1 = fe.DirichletBC(problem.functionSpace.sub(0), fe.Constant(0), lambda x, on_boundary: not fe.between(x[0], (-pos+eps, pos-eps)))
    problem.add_boundary_condition(walls_1)

    walls_2 = fe.DirichletBC(problem.functionSpace.sub(1), fe.Constant(0), lambda x, on_boundary: not fe.between(x[1], (-pos+eps, pos-eps)))
    problem.add_boundary_condition(walls_2)

    d4 = Domain('near(z,0)')
    diri.append(ConstantDirichlet(0, d4, subspace=2))

    problem.add_boundary_condition(diri)



def hyperelasticity(mesh, pressure, mu, lmbda, bnd_cond="no_walls", pos_wall=1):

    problem = HyperElasticityProblem()
    problem.solver_parameter(absolute_tolerance=1e-7, monitor_convergence=True,
                            linear_solver='gmres', preconditioner='ilu')
    problem.set_mesh(mesh)
    problem.set_function_space('P', 1)

    if bnd_cond=="no_walls":
        no_walls(problem)
    elif bnd_cond=="walls2":
        walls2(problem, pos=pos_wall)
    elif bnd_cond=="walls4":
        walls4(problem, pos=pos_wall)

    problem.set_material_model('st_venant', mu, lmbda)

    P = pressure_forces(problem.mesh, pressure)
    problem.set_body_force(P)

    p_init = fe.project(P, problem.functionSpace)
    problem.solution.assign(P)

    problem.solve()

    return problem.solution



def simulate(mesh, pressure, mu, lmbda, repeat=1, bnd_cond="walls", result_dir=None):
    print('-'*40)
    print('*** Launching HyperElastic calculus ***\n')
    print('Number of repetition:', repeat,'\n')
    print('Mesh:')
    print('\tTriangles:', mesh.num_entities(2))
    ratio_mesh = mesh.coordinates()[:,2].max()/mesh.coordinates()[:,1].max()
    print('\tRatio    :',ratio_mesh,'\n')
    print('Parameters:')
    print('\tPressure: ', pressure)
    print('\tMu:       ', mu)
    print('\tLambda:   ', lmbda)
    print('\tBoundary: ', bnd_cond)
    print('-'*40,'\n')

    pos_wall = mesh.coordinates()[:, 0].max()

    # file_disp  =fe.XDMFFile(result_dir+'/displacement.xdmf')
    # file_disp.parameters["flush_output"] = True
    # file_disp.parameters["rewrite_function_mesh"] = False

    for i in range(repeat):
        print(f'Step {i+1}/{repeat}:')
        print('\tSolving hyperelasticity ... ', end='')
        start = time.time()
        disp = hyperelasticity(mesh, pressure, mu, lmbda, bnd_cond=bnd_cond, pos_wall=pos_wall)
        end = time.time()
        print(f'in [{end-start} s]')

        if result_dir is not None:
            print('\tPostprocessing ... ', end='')
            start = time.time()

            with fe.XDMFFile(result_dir+'/mesh_0.xdmf') as f:
                f.write(mesh)

            T = fe.TensorFunctionSpace(mesh, 'DG', 0)

            strain = fe.project(compute_strain(disp), T)
            with fe.XDMFFile(result_dir+'/strain_'+str(i)+'.xdmf') as f:
                f.write_checkpoint(strain, "strain", 0)

            # with fe.XDMFFile(result_dir+'/displacement_'+str(i)+'.xdmf') as f:
            #     f.write_checkpoint(disp, "displacement", 0)

            move_mesh(mesh, disp)

            T = fe.TensorFunctionSpace(mesh, 'DG', 0)
            cauchy = fe.Function(T, name="cauchy")
            expr_caucchy = compute_cauchy(disp, mu, lmbda, projection=True)
            fe.project(expr_caucchy, T, function=cauchy)

            with fe.XDMFFile(result_dir+'/cauchy_'+str(i+1)+'.xdmf') as f:
                f.write_checkpoint(cauchy, "cauchy", 0)

            with fe.XDMFFile(result_dir+'/cauchy_sym_'+str(i+1)+'.xdmf') as f:
                sym_cauchy = project_sym_tensor(cauchy, mesh, function_type='DG', function_degree=0, name='cauchy')
                f.write(sym_cauchy, 0)

            aniso_cauchy = compute_anisotropy(cauchy)

            with fe.XDMFFile(result_dir+'/anisotropy_'+str(i+1)+'.xdmf') as f:
                f.write_checkpoint(aniso_cauchy, "anisotropy", 0)

            orientation_cauchy = measurement_orientation(cauchy)

            with fe.XDMFFile(result_dir+'/orientation_'+str(i+1)+'.xdmf') as f:
                f.write_checkpoint(orientation_cauchy, "orientation", 0)

            tension_compression = discriminate_tension_compression(cauchy)

            with fe.XDMFFile(result_dir+'/tension_compression_'+str(i+1)+'.xdmf') as f:
                f.write_checkpoint(tension_compression, "tension_compression", 0)

            eigv = eigvals(cauchy)

            with fe.XDMFFile(result_dir+'/eigvals_cauchy_'+str(i+1)+'.xdmf') as f:
                f.write_checkpoint(eigv, "eigvals_cauchy", 0)

            with fe.XDMFFile(result_dir+'/mesh_'+str(i+1)+'.xdmf') as f:
                f.write(mesh)

            end = time.time()
            print(f'in [{end-start} s]')
