import fenics as fe
import numpy as np



def compute_anisotropy(tensor, function=True):
    array = tensor.vector().get_local().reshape(-1, 3, 3)
    eigvalue, eigvec = np.linalg.eigh(array)

    eigvalue = eigvalue[:,1:]
    anisotropy = [(max(eigs)-min(eigs))/sum(eigs) for eigs in eigvalue]

    V = fe.FunctionSpace(tensor.function_space().mesh(), 'DG', 0)
    f = fe.Function(V, name='anisotropy')
    f.vector().set_local(np.array(anisotropy))

    return f


def measurement_orientation(tensor, eps=1e-9):
    array = tensor.vector().get_local().reshape(-1, 3, 3)
    eigval, eigvec = np.linalg.eigh(array)
    eigval_2 = eigval[:, 1:]
    eigvec_2 = eigvec[:, :, 1:]

    p = np.array([v1*e1 - e2*v1 if np.linalg.norm(v1*e1 - e2*v1)>eps else np.zeros(3) for v1, e2, e1 in  zip(eigvec_2[:,:,1],eigval_2[:,0], eigval_2[:,1])])

    V = fe.VectorFunctionSpace(tensor.function_space().mesh(), 'DG', 0)
    f = fe.Function(V, name='orientation')
    f.vector().set_local(np.array(p.flatten()))

    return f


def decimate_tensor(tensor):
    mesh = tensor.function_space().mesh()
    array = tensor.vector().get_local().reshape(-1,3)
    center_tri = np.array([c.midpoint().array() for c in fe.cells(mesh)])
    cell_list = np.arange(mesh.num_entities(2)).tolist()
    to_remove = []
    while len(cell_list)!=0:
        ind = cell_list[0]
        neighbors = cell_neigbors(fe.Cell(mesh, ind))

        to_remove += neighbors

        cell_list.remove(ind)
        for i in neighbors:
            try:
                cell_list.remove(i)
            except:
                pass

    to_remove = np.unique(to_remove)
    array = np.delete(array, to_remove, axis=0)
    center_tri = np.delete(center_tri, to_remove, axis=0)


def evaluate_tensor(point, tensor):
    return tensor(point).reshape(tensor.ufl_shape)

def cell_neigbors(cell):
    cells_ind = [c.index() for e in fe.edges(cell) for c in fe.cells(e) if c.index() != cell.index()]
    return cells_ind
