=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* moi, <moi@email.com>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Florian <florian.gacon@gmail.com>
* GACON Florian <florian.gacon@inria.fr>

.. #}
